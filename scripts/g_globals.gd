# -----------------------------------------------------------------
# Class:          		Globals
# Author:         		Vegevan                    
# Notes:          		Singleton
# Date:					04/2017
# Description:			Globals singleton
# -----------------------------------------------------------------


extends Node2D

# Managers
var managers_scn = preload("res://scenes/managers/managers.tscn")

# Window Dimension
onready var kWindowWidth = Globals.get("display/width")
onready var kWindowHeight = Globals.get("display/height")

# The manager service locator
var managers

# The global music engine
var music_engine

# The global sound engine
var sound_engine

# The global modulate
var modulate

# The global animation
var animation

# The bullet pool
var bullet

#
# Globals initialization
#
func _ready():
	add_child(managers_scn.instance())
	
	managers = get_node("managers")
	music_engine = managers.get_node("music")
	sound_engine = managers.get_node("sound")
	modulate = managers.get_node("modulate")
	animation = managers.get_node("animation")
	bullet = managers.get_node("bullet")
	
	set_process(true)
	

const BOSS_TIME = 5 * 60
var boss_counter = 0
var boss_scene = false

func _process(delta):
	if g_player.player != null:
		boss_counter += delta
		if boss_counter >= BOSS_TIME and boss_scene == false:
			g_scene_manager.goto_scene("boss")
			boss_scene = true
	
	
# BOSS
var boss = null
var boss_health = null