# -----------------------------------------------------------------
# Class:          		g_loader
# Author:         		Vegevan
# Notes:          		Singleton
# Date:					04/2017
# Description:			Resource Loader
# -----------------------------------------------------------------

extends Node2D

# Max time spent loading
const k_max_loading_time = 10 # seconds

# The duration before showing loading bar
const k_minimum_duration_before_loading_scene = 1;

# Signal called when a loading is finished
signal loading_finished

# The scene displayed while loading
var loading_node = preload("res://scenes/loading/loading.tscn").instance()

# The loader, use load_interactive()
var loader = null

# The result of the loading
var resource_loaded = null

# The load time
var load_time = 0

# Is the loading bar displayed
var is_loading_bar_displayed = false

#
#	True if loading
#
func is_loading():
	return loader != null

#
#	Update
#
func _process(delta):
    # poll your loader
	assert(loader)

	# update load time
	load_time += delta
	
	# Print loading bar
	if load_time >= k_minimum_duration_before_loading_scene and is_loading_bar_displayed == false:
		is_loading_bar_displayed = true
		add_child(loading_node)

	# Timeout
	if load_time >= k_max_loading_time:
		print("[Loader]:\tLoad timeout : ", k_max_loading_time, ". Aborted.")
		set_process(false)
		return

	# Loading Status
	var status = loader.poll()

	# Load ended
	if status == ERR_FILE_EOF: # load finished
		if is_loading_bar_displayed == true:
			get_node("loading/bar").set_max( loader.get_stage_count() )
			get_node("loading/bar").set_val( loader.get_stage() )

		# Update result
		resource_loaded = loader.get_resource()
		load_time = 0
		loader = null
		
		if is_loading_bar_displayed == true:
			remove_child(loading_node)
			is_loading_bar_displayed = false

		# Send result
		emit_signal("loading_finished", resource_loaded)

		# Debug
		print("[Loader]:\tInteractive load succeeded !")

	# Loading
	elif status == OK:
		if is_loading_bar_displayed == true:
			get_node("loading/bar").set_max( loader.get_stage_count() )
			get_node("loading/bar").set_val( loader.get_stage() )

	# Error
	else:
		loader = null
		
		if is_loading_bar_displayed == true:
			remove_child(loading_node)

		# Debug
		print("[Loader] ERROR:\tInteractive load failed !")
		yield()

#
#	Commend a loading
#
func load_interactive( path ):
	# Enable process
	set_process(true)
	g_globals.animation.play("fade_to_dark")

	# Connect to loading finished  signal
	connect("loading_finished", self, "_on_loading_finished")
	loader = ResourceLoader.load_interactive(path)

#
#	Callback when loading is finished
#
func _on_loading_finished(loaded):
	# Disable process
	set_process(false)
	g_globals.animation.play("fade_to_light")

	# Disconnect the loading finished signal
	disconnect("loading_finished", self, "_on_loading_finished")
