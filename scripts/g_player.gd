# -----------------------------------------------------------------
# Class:          		g_player
# Author:         		Vegevan                    
# Notes:          		Singleton
# Date:					04/2017
# Description:			Player information
# -----------------------------------------------------------------

extends Node2D

# Player object
var player

# Health management
var HEALTH = 2
var health = HEALTH
signal health_changed

var invincible_end = -1
var invincible_counter = 0

# POWER
var power = 1
var charge = 0

var praying_counter = 0
var PRAYING_EFFECT = 2

func take_damage(p_damage):
	if invincible_counter >= invincible_end:
		health = clamp(health - p_damage, 0, HEALTH)
		emit_signal("health_changed", health, p_damage)
		set_invicible(1)
	
func set_invicible(p_duration):
	invincible_end = p_duration
	invincible_counter = 0
	
func _process(delta):
	invincible_counter += delta
	if player != null and player.is_praying == true:
		praying_counter += delta
		print("Praying : " , praying_counter , "/", PRAYING_EFFECT)
		if praying_counter >= PRAYING_EFFECT:
			charge += 1
			praying_counter = 0

# XP management
var level = 1
var xp = 0
signal level_up

func xp_to_next_level():
	return pow(level + 2, 2)
	
func _ready():
	add_to_group("xp")
	set_process(true)
	
	var buildings = get_tree().get_nodes_in_group("building")
	for building in buildings:
		building.get_node("sensor").connect("captured", self, "_on_building_captured")

func gain_xp(p_xp):
	xp += p_xp
	if xp >= xp_to_next_level():
		xp = 0
		level += 1
		HEALTH += 1
		health += 1
		emit_signal("health_changed", health, -1)
		emit_signal("level_up", level)
		if level % 2 == 0:
			power += 1
		
		
# BUILDINGS
var cimetery = false
var church = false

func _on_building_captured(p_building, p_by_who):
	if p_by_who != "player": return
	
	if p_building.get_name() == "cimetery":
		cimetery = true
	if p_building.get_name() == "church":
		church = true
	