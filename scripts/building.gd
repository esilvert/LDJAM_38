# -----------------------------------------------------------------
# Class:          		building
# Author:         		Vegevan                    
# Notes:          		Singleton
# Date:					04/2017
# Description:			Building logic
# -----------------------------------------------------------------
export(String) var name

onready var sensor = get_node("sensor")

var neutral
var angel
var evil

var states = {
			"neutral" : {"action" : "execute_neutral", "first_frame" : false, "texture" : neutral},
			"angel" : {"action" : "execute_angel", "first_frame" : false, "texture" : angel},
			"evil" : {"action" : "execute_evil", "first_frame" : false, "texture" : evil}
			}

var m_current_state = states["neutral"]
var m_state_time = 0

var possessor = "neutral"

func _ready():
	set_process(true)
	add_to_group("building")
	get_node("sensor").connect("captured", self, "_on_captured")

func _process(delta):
	if neutral == null: neutral = load("res://scenes/buildings/"+name+"_neutral.png")
	if evil == null: evil = load("res://scenes/buildings/"+name+"_evil.png")
	if angel  == null: angel = load("res://scenes/buildings/"+name+"_angel.png")
	
	
#	call(m_current_state["action"], delta)
	_on_post_action(delta)
	
func _on_post_action(delta):
	m_current_state["first_frame"] = m_state_time == 0
	m_state_time += delta
	
	if sensor.player_influence == 1:
		possessor = "player"
	elif sensor.devil_influence == 1:
		possessor = "devil"
	else:
		possessor = "neutral"
	
	if possessor == "player" and sensor.player_influence >= 0.5:
		set_texture(angel)
	elif possessor == "devil" and sensor.devil_influence >= 0.5:
		set_texture(evil)
	else:
		set_texture(neutral)
	
func switch_state(p_next_state):
	m_current_state = states[p_next_state]
	m_current_state["first_frame"] = true
	set_texture(m_current_state["texture"])
	m_state_time = 0
	
func _on_captured(p_building, p_by_who):
	possessor = p_by_who
	if p_by_who == "enemy" and m_current_state != states["evil"]:
		switch_state("evil")
		get_tree().call_group("spawner", name + "_captured")
	elif p_by_who == "player" and m_current_state != states["angel"]:
		switch_state("angel")
		
func _exit_tree():
	remove_from_group("building")