extends Area2D


export var TIME_TO_CAPTURE = 10
var player_capture_counter = 0
var player_present = false
var player_influence = 0

var devil_capture_counter = 0
var devil_present = false
var devil_influence = 0
signal captured

onready var player_bar = get_node("../player_bar")
onready var devil_bar = get_node("../devil_bar")

func _ready():
	connect("body_enter", self, "_on_body_enter")
	connect("body_exit", self, "_on_body_exit")
	player_bar.set_max(TIME_TO_CAPTURE)
	devil_bar.set_max(TIME_TO_CAPTURE)
	player_bar.set_value(0)
	devil_bar.set_value(0)
	
func _on_body_enter(p_body):
	if p_body.is_in_group("player") and player_present == false:
		set_process(true)
		player_present = true
	if p_body.is_in_group("devil") and devil_present == false:
		set_process(true)
		devil_present = true

func _on_body_exit(p_body):
	if p_body.is_in_group("player"):
		player_present = false
	if p_body.is_in_group("devil"):
		devil_present = false
	
func _process(delta):
	var player_inc = 0
	var devil_inc = 0
	var possessor = get_parent().possessor
	
	if possessor != "player" and (player_present == false or g_player.player.is_praying == false): player_inc = -delta
	if possessor != "devil" and devil_present == false: devil_inc = -delta
	
	if player_present and g_player.player.is_praying and not devil_present:
		player_inc = delta
	if devil_present and not player_present:
		devil_inc = delta
	
	if possessor == "player":
		player_inc = 0
	if possessor == "devil":
		devil_inc = 0
	
	player_capture_counter = clamp(player_capture_counter + player_inc, 0 , TIME_TO_CAPTURE)
	devil_capture_counter = clamp(devil_capture_counter + devil_inc, 0, TIME_TO_CAPTURE)
	
	
	player_influence = min(1, player_capture_counter / TIME_TO_CAPTURE)
	player_bar.set_value(player_capture_counter)
	
	devil_influence = min(1, devil_capture_counter / TIME_TO_CAPTURE)
	devil_bar.set_value(devil_capture_counter)
	
	if player_influence == 1 and get_parent().possessor != "player":
		emit_signal("captured", get_parent(), "player")
#		devil_influence = 0
	elif devil_influence == 1 and get_parent().possessor != "devil":
		emit_signal("captured", get_parent(), "devil")
#		player_influence = 0
