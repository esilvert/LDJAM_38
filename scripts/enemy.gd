# -----------------------------------------------------------------
# Class:          		Enemy
# Author:         		Vegevan                    
# Notes:          		Singleton
# Date:					04/2017
# Description:			Enemies behavior
# -----------------------------------------------------------------

extends RigidBody2D

#
#	Ready
#
func _ready():
	set_linear_velocity(Vector2(30,0))
