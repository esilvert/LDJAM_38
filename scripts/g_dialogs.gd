extends Node2D

var dialogs = {
	"intro1" : {
		"texture" : "res://assets/textures/devil_preview.png",
		"text" : "...",
		"next" : "intro2",
		"sound" : "intro1"
	},
	
	"intro2" : {
		"text" : "Well well well, this looks like a pretty small world.",
		"next" : "intro3",
		"sound" : "intro2"
	},
	
	"intro3" : {
		"text" : "I'm gonna own it.",
		"next" : "intro4",
		"sound" : "intro3"
	},
	
	"intro4" : {
		"texture" : "res://assets/textures/player_preview.png",
		"text" : "God protects this world, I won't let you do that !",
		"next" : "intro5",
		"sound" : "intro4"
	},
	
	"intro5" : {
		"texture" : "res://assets/textures/devil_preview.png",
		"text" : "What ?! Who are you ! You can't even fly ! Hahaha.",
		"next" : "intro6",
		"sound" : "intro5"
	},
	
	"intro6" : {
		"texture" : "res://assets/textures/player_preview.png",
		"text" : "Well, I'm in charge of this world. You are pityful, I won't even use a weapon.",
		"next" : "intro7",
		"sound" : "intro6"
	},
	
	"intro7" : {
		"texture" : "res://assets/textures/devil_preview.png",
		"text" : "Whaaat ?! I am the mighty Kra'kh, you're gonna regret it !",
		"call_group_and_method" : "tuto",
		"sound" : "intro7"
	},
}