# -----------------------------------------------------------------
# Class:          		Spawner
# Author:         		Vegevan                    
# Notes:          		Singleton
# Date:					04/2017
# Description:			Spawn enemies
# -----------------------------------------------------------------


extends Node2D

# Time between two spawns
export var SPAWN_PERIOD = 2
var m_spawn_counter = 0

# Enemy
export(String) var enemy_name
var enemy_scn 

export var enabled = false
export var CONNECTION_BUILDING = "../church"

var spawn_points = []

func _ready():
	enemy_scn = load("res://scenes/enemies/"+enemy_name+".tscn")
	spawn_points = get_children()
	assert(spawn_points.size() != 0)
	set_process(enabled)
	get_node(CONNECTION_BUILDING + "/sensor").connect("captured", self, "_on_capture")

#
#	Update
#
func _process(delta):
	m_spawn_counter += delta
	
	if m_spawn_counter >= SPAWN_PERIOD:
		do_spawn()
		m_spawn_counter = 0

#
#	Spawn an ENY
#
func do_spawn():
	# Chose random point
	var spawn_point = spawn_points[ rand_range(0, spawn_points.size()) ]
	spawn_point.add_child(enemy_scn.instance())

func _on_capture(p_what, p_by_who):
	if p_by_who == "devil":
		if p_what.get_name() == "cimetery": cimetery_captured()
		if p_what.get_name() == "church": church_captured()

func cimetery_captured():
	if enemy_name == "skeleton":
		enabled = true
		set_process(true)

func church_captured():
	if enemy_name == "satanist":
		enabled = true
		set_process(true)