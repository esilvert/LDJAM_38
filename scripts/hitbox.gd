extends Area2D

export var DAMAGE = 1
export var NUMBER_HIT = -1
var number_hit = 0

export var TARGET_PLAYER = true
export var TARGET_ENEMY = false

signal on_hit

func _ready():
	connect("body_enter", self, "_on_body_enter")
	if g_player.cimetery == true:
		NUMBER_HIT += 2

func _on_body_enter(p_body):
	if TARGET_PLAYER == true and p_body.is_in_group("player"):
		handle_hit(p_body)
		
	if TARGET_ENEMY == true and p_body.is_in_group("enemy"):
		handle_hit(p_body)

func handle_hit(p_body):
	if TARGET_PLAYER == true:
		p_body.health.take_damage(g_player.power)
	else:
		p_body.health.take_damage(DAMAGE)
	emit_signal("on_hit", p_body)
	
	number_hit += 1
	if number_hit >= NUMBER_HIT && NUMBER_HIT != -1:
		get_parent().queue_free()