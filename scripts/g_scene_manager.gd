# -----------------------------------------------------------------
# Class:          		g_scene_manager
# Author:         		Vegevan                    
# Notes:          		Singleton
# Date:					04/2017
# Description:			Changing scene
# -----------------------------------------------------------------

extends Node2D

#
#	Ready
#
func _ready():
	pass

#
#	Change current scene with interactive load
#
func goto_scene( p_name ) :
	g_loader.load_interactive("res://scenes/" + p_name + "/" + p_name + ".tscn")
	g_loader.connect("loading_finished", self, "_on_loading_finished")

#
#	Callback when loading done
#
func _on_loading_finished(scene):
	# Forget loader
	g_loader.disconnect("loading_finished", self, "_on_loading_finished")

	# Free current scene
	get_tree().get_current_scene().queue_free()

	# Instantiate new scene and set current
	var scene_instance = scene.instance()
	get_tree().get_root().add_child(scene_instance)
	get_tree().set_current_scene(scene_instance);